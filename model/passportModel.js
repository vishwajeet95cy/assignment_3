const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  tokens: {
    type: Array
  },
  email: {
    type: String
  },
  password: {
    type: String
  },
  createdAt: {
    type: Date,
    default: new Date
  },
  updatedAt: {
    type: Date,
    default: new Date
  },
  emailVerficationToken: {
    type: String
  },
  profile: {
    type: Object
  },
  google: {
    type: String
  },
  facebook: {
    type: String
  }
})


module.exports = mongoose.model('Credential', UserSchema)