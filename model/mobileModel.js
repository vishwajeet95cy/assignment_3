const mongoose = require('mongoose');

const methodSchema = new mongoose.Schema({
  uuid: {
    type: String,
    required: true
  },
  iemi: {
    type: String,
    required: true
  },
  callid: {
    type: String,
    required: true
  },
  auth: {
    type: String,
    required: true
  },
  Date: {
    type: Date,
    default: new Date
  }
})

module.exports = mongoose.model('mobileData', methodSchema)