var mongoose = require('mongoose');
var Schema = mongoose.Schema;

let getId = () => {
  return '_' + Math.random().toString(36).substr(2, 9);
}

var schema = new Schema({
  name: { type: String, required: true },
  age: { type: Number, required: true },
  email: { type: String, required: true },
  role: { type: String, default: 'user', enum: ['admin', 'user'] },
  apiKey: { type: String, default: getId() },
  date: { type: Date, default: new Date }
});

module.exports = mongoose.model('UserRoles', schema);