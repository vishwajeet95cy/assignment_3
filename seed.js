var UserRoles = require('./model/userRole');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/', { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
  if (err) throw err;
  console.log('Database Connect');
})

UserRoles.deleteMany({}, function () {
  console.log('Database Cleared');
});

var userroles = [
  new UserRoles({
    name: 'Vishwajeet Kumar',
    age: 20,
    email: 'vkskpkk@gmail.com',
    role: 'admin'
  }),
  new UserRoles({
    name: 'Vikash Kumar',
    age: 22,
    email: 'vikash25@gmail.com',
  }),
  new UserRoles({
    name: 'Shiva Kumar',
    age: 22,
    email: 'shiva22@gmail.com',
  })
];

var done = 0;
for (var i = 0; i < userroles.length; i++) {
  userroles[i].save(function (err, result) {
    if (err) {
      console.log(err);
      return;
    };

    done++;
    if (done === userroles.length) {
      exist()
    };
  });
};

function exist() {
  mongoose.disconnect()
}