const router = require('express').Router();
const passport = require('passport')
const { register, login, streamVideo, method, userDetails } = require('../Controllers/user_auth');
require('../controllers/passort-setup');


/**
 * @swagger
 * /auth/userDetails:
 *   get:
 *     description: Use to request all user with roles
 *     produces:
 *       - application/json
 *     responses:
 *        200:
 *           Description:A successful response
 */

/**
* @swagger
* /auth/register:
*   post:
*     summary: Create a user
*     produces:
*       - application/json
*       - x-www-form-urlencoded
*     parameters:
*        - in: body
*          name: user
*          description: The user to create
*          schema:
*           type: object
*           required:
*            - name
*            - email
*            - password
*           properties:
*             name:
*              type: string
*             email:
*              type: string
*             password:
*              type: string
*     responses:
*        200:
*           description:User Created
*/

/**
* @swagger
* /auth/login:
*   post:
*     summary: Login a user
*     produces:
*       - application/json
*       - x-www-form-urlencoded
*     parameters:
*        - in: body
*          name: user
*          description: The user to login
*          schema:
*           type: object
*           required:
*            - email
*            - password
*           properties:
*             email:
*              type: string
*             password:
*              type: string
*     responses:
*        200:
*           description:Login Created
*/

/**
* @swagger
* /auth/onCall:
*   post:
*     summary: Create a Mobile Model
*     produces:
*       - application/json
*       - x-www-form-urlencoded
*     parameters:
*        - in: body
*          name: user
*          description: The Mobile model to create
*          schema:
*           type: object
*           required:
*            - uuid
*            - iemi
*            - callid
*            - auth
*           properties:
*             uuid:
*              type: string
*             iemi:
*              type: string
*             callid:
*              type: string
*             auth:
*              type: string
*     responses:
*        200:
*           description:Login Created
*/

router.post('/register', register);

router.post('/login', login);

router.get('/stream', streamVideo);

router.post('/onCall', method);

router.get('/userDetails', userDetails)

router.get('/google',
  passport.authenticate('google', {
    scope: ['profile', 'email', 'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email'], accessType: 'offline', prompt: 'consent'
  }));

router.get('/google/callback',
  passport.authenticate('google', { failureRedirect: '/failed' }),
  function (req, res) {
    res.redirect('/good');
  });

router.get('/logout', (req, res, next) => {
  req.session.destroy((error) => {
    console.log(error)
  })
  req.logout();
  res.redirect('/')
})

router.get('/facebook', passport.authenticate('facebook', {
  scope: ['email', 'public_profile']
}));

router.get('/facebook/callback',
  passport.authenticate('facebook', {
    successRedirect: '/good',
    failureRedirect: '/failed'
  }));

router.post('/onplay', (req, res, next) => {
  // grab the id from the request
  const socketId = req.body.socketId
  console.log(req.headers.cookie)
  console.log(req.body);
  console.log(req.body.message)
  console.log(socketId)
  // get the io object ref
  const io = req.app.get('socketio')
  // console.log(io)

  // create a ref to the client socket
  const senderSocket = io.sockets.connected[socketId]
  console.log(senderSocket)

  io.emit('onplay', { message: req.body.message })
  res.status(201).json({ message: req.body.message })

  // Message.create(req.body.message)
  //   .then(message => {

  //     // in case the client was disconnected after the request was sent
  //     // and there's no longer a socket with that id
  //     if (senderSocket) {

  //       // use broadcast.emit to message everyone except the original
  //       // sender of the request !!! 
  //       senderSocket.emit('onplay', { message })
  //     }
  //     res.status(201).json({ message: message.toObject() })
  //   })
  //   .catch(next)
})


module.exports = router;