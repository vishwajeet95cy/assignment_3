const passport = require('passport');
const bcrypt = require('bcryptjs')
const { OAuth2Strategy: GoogleStrategy } = require('passport-google-oauth');
const { Strategy: FacebookStrategy } = require('passport-facebook');
const Credential = require('../model/passportModel')

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  Credential.findById(id, function (err, user) {
    done(err, user);
  });
});

passport.use('google', new GoogleStrategy({
  clientID: process.env.clientID,
  clientSecret: process.env.clientSecret,
  callbackURL: "http://localhost:8000/auth/google/callback",
  passReqToCallback: true
},
  (request, accessToken, refreshToken, params, profile, done) => {
    // console.log(profile);
    // console.log("user " + request.user);
    // console.log(accessToken);
    // console.log(refreshToken);
    // console.log(params);
    // const salt = bcrypt.genSalt(10);
    // const hasedPass = await bcrypt.hash(request.body.password, salt);
    Credential.findOne({ google: profile.id }).then((currentUser, profile) => {
      if (currentUser) {
        console.log('user is:' + currentUser);
        done(null, currentUser)
        // Credential.updateOne({ tokens: [{ kind: profile.provider, TokenType: params.token_type, AccessToken: accessToken, RefreshToken: refreshToken, accessTokenExpires: new Date().toLocaleString(params.expires_in) }] },).then((currentUser) => {
        //   console.log('Data is :' + currentUser);
        //   done(null, currentUser)
        // }).catch((err) => {
        //   console.log(err);
        // })
      } else {
        new Credential({
          profile: {
            location: profile._json.locale,
            name: profile.displayName,
            website: '',
            photos: profile._json.picture
          },
          tokens: [{
            kind: profile.provider,
            TokenType: params.token_type,
            AccessToken: accessToken,
            RefreshToken: refreshToken,
            accessTokenExpires: new Date().toLocaleString(params.expires_in)
          }],
          emailVerficationToken: "",
          email: profile.emails[0].value,
          password: "",
          google: profile.id
        }).save().then((data) => {
          console.log('New User created' + data);
          done(null, data)
        }).catch((err) => {
          console.log(err)
        })
      }
    }).catch((err) => {
      console.log(err)
    });
  }
));

passport.use('facebook', new FacebookStrategy({
  clientID: process.env.fbid,
  clientSecret: process.env.fbsecret,
  callbackURL: "http://localhost:8000/auth/facebook/callback",
  passReqToCallback: true,
  profileFields: ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone',
    'updated_time', 'verified', 'displayName']
},
  (request, accessToken, refreshToken, params, profile, done) => {
    // console.log(profile);
    // console.log("user " + request.user);
    // console.log(accessToken);
    // console.log(refreshToken);
    // console.log(params);
    Credential.findOne({ facebook: profile.id }).then((currentUser) => {
      if (currentUser) {
        console.log('user is:' + currentUser);
        done(null, currentUser)
      } else {
        new Credential({
          profile: {
            location: '',
            name: profile.displayName,
            website: '',
            photos: ''
          },
          tokens: [{
            kind: profile.provider,
            TokenType: params.token_type,
            AccessToken: accessToken,
            RefreshToken: refreshToken,
            accessTokenExpires: new Date().toLocaleString(params.expires_in)
          }],
          emailVerficationToken: "",
          email: profile.emails[0].value,
          password: "",
          facebook: profile.id
        }).save().then((data) => {
          console.log('New User created' + data);
          done(null, data)
        }).catch((err) => {
          console.log(err)
        })
      }
    }).catch((err) => {
      console.log(err)
    });
  }
));

module.exports.passport = passport