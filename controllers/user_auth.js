const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const UserData = require('../model/userModel.js');
const mobileData = require('../model/mobileModel')
const fs = require('fs');
const UserRoles = require('../model/userRole')

const { registerValidation, loginValidation } = require('../validation');

const register = async (req, res) => {
  // validate data
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // checking if the user is already in the database
  const emailExist = await UserData.findOne({ email: req.body.email });
  if (emailExist) return res.status(400).send('Email already exist');

  // Hash passwords
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  // create a new user
  const user = new UserData({
    name: req.body.name,
    email: req.body.email,
    password: hashedPassword
  });
  try {
    const savedUser = await user.save();
    res.send({ user: user._id, user: savedUser });
  } catch (err) {
    res.status(400).send(err);
  }
};

const login = async (req, res) => {

  // validate data
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // checking if the email exists
  const user = await UserData.findOne({ email: req.body.email });
  if (!user) return res.status(400).send('Email is not found');

  // password is correct
  // checking if the user is already in the database
  const validPass = await bcrypt.compare(req.body.password, user.password)
  if (!validPass) return res.status(400).send('Invalid Password');

  //Create and assign a token
  const token = jwt.sign({ _id: user._id, name: user.name }, process.env.Token_secret);
  res.header('auth-token', token).send(token);
};

const streamVideo = (req, res) => {
  const path = './assests/queries.mp4';
  const stat = fs.statSync(path);
  const fileSize = stat.size;
  const range = req.headers.range;
  if (range) {
    const parts = range.replace(/bytes=/, "").split("-");
    const start = parseInt(parts[0], 10);
    const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
    const chunkSize = (end - start) + 1;
    const file = fs.createReadStream(path, { start, end });
    const head = {
      'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      'Accept-Ranges': 'bytes',
      'Content-Length': chunkSize,
      'Content-Type': 'video/mp4'
    }
    res.writeHead(206, head);
    file.pipe(res);
  } else {
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'video/mp4'
    }
    res.writeHead(200, head);
    fs.createReadStream(path).pipe(res);
  }
}

const userDetails = (req, res) => {
  UserRoles.find().then((data) => {
    res.status(200).send(data)
  }).catch((err) => {
    console.log(err)
  })
}

const method = (req, res) => {
  if (!req.body.uuid || !req.body.iemi || !req.body.callid || !req.body.auth) {
    return res.status(400).send('Invalid Details')
  }
  const item = new mobileData({
    uuid: req.body.uuid,
    iemi: req.body.iemi,
    callid: req.body.callid,
    auth: req.body.auth
  })
  item.save().then((data) => {
    res.status(200).json({
      success: "Data added successfully",
      url: 'http://' + req.hostname + ':' + process.env.PORT + req.originalUrl,
      auth: data.auth
    })
  }).catch((err) => {
    console.log(err)
  })
}

function authenticate(socket, data, callback) {
  // var username = data.username;
  // var password = data.password;
  UserData.findOne({ email: data.username }, async (err, user) => {
    if (err || !user) return callback(new Error("User not found"));
    const pass = await bcrypt.compare(data.password, user.password);
    if (!pass) return callback(new Error("Invalid Password"))
    return callback(null, pass);
  });
}

function postAuthenticate(socket, data) {
  // var username = data.username;

  UserData.findOne({ email: data.username }, function (err, user) {
    socket.client.user = user;
  });
}

function disconnect(socket) {
  console.log(socket.id + ' disconnected');
}

module.exports.register = register;
module.exports.login = login;
module.exports.streamVideo = streamVideo;
module.exports.method = method;
module.exports.authenticate = authenticate;
module.exports.postAuthenticate = postAuthenticate;
module.exports.disconnect = disconnect;
module.exports.userDetails = userDetails;
