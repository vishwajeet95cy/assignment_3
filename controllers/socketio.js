const mobileData = require('../model/mobileModel')
const socket = require('socket.io')()

const data = (data) => {
  if (!data.uuid || !data.iemi || !data.callid || !data.auth) {
    // return res.status(400).send('Invalid Details')
    return console.log("Invalid Data");
  }
  new mobileData({
    uuid: data.uuid,
    iemi: data.iemi,
    callid: data.callid,
    auth: data.auth
  }).save((err, docs) => {
    if (err) { return console.log("error"); }
    socket.emit('call', {
      success: "Data added successfully",
      url: 'http://' + data.hostname + ':' + process.env.PORT + data.originalUrl,
      auth: docs.auth,
      done: 'Done',
      data: docs
    })
    // socket.emit('callback', { done: 'Done', data: docs })
  })
}

module.exports = { data }


