module.exports = function (io) {
  io.on("connection", function (socket) {
    console.log('connected')
    socket.on('data', (data) => {
      if (!data.uuid || !data.iemi || !data.callid || !data.auth) {
        return console.log("Invalid Data");
      }
      new mobileData({
        uuid: data.uuid,
        iemi: data.iemi,
        callid: data.callid,
        auth: data.auth
      }).save((err, docs) => {
        console.log(docs)
        if (err) { return console.log("error"); }
        socket.emit('callback', {
          success: "Data added successfully",
          url: 'http://' + data.hostname + ':' + process.env.PORT + data.originalUrl,
          auth: docs.auth,
          done: 'Done',
          data: docs
        })
        // socket.emit('callback', { done: 'Done', data: docs })
      })
    });
    socket.on('disconnect', () => {
      console.log('disconnect')
    })
  });
};


// io.use((socket, next) => {
//   console.log(socket.handshake)
//   let token = socket.handshake.query.token;
//   if (!token) {
//     return next();
//   }
//   return next(new Error('authentication error'));
// });


// socketAuth(io, {
//   authenticate: authenticate,
//   postAuthenticate: postAuthenticate,
//   disconnect: disconnect,
//   timeout: 1000
// })

// io.on('connection', (socket) => {
//   console.log('Connecteds')
//   socket.on('data', (data) => {
//     if (!data.uuid || !data.iemi || !data.callid || !data.auth) {
//       return console.log("Invalid Data");
//     }
//     new mobileData({
//       uuid: data.uuid,
//       iemi: data.iemi,
//       callid: data.callid,
//       auth: data.auth
//     }).save((err, docs) => {
//       console.log(docs)
//       if (err) { return console.log("error"); }
//       socket.emit('callback', {
//         success: "Data added successfully",
//         url: 'http://' + data.hostname + ':' + process.env.PORT + data.originalUrl,
//         auth: docs.auth,
//         done: 'Done',
//         data: docs
//       })
//       // socket.emit('callback', { done: 'Done', data: docs })
//     })
//   });
// })

// io.use(socketJWT.authorize({
//   secret: process.env.Token_secret,
//   handshake: true
// }));

// io.on('connection', (socket) => {
//   console.log('Io on method connect');
//   console.log(socket.decoded_token);
//   console.log('hello!', socket.decoded_token.name);
//   socket.emit('done', {
//     data: socket.decoded_token,
//     done: 'Done'
//   })
// })