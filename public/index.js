const socket = io()
let token = '';

socket.on('connect', function () {
  console.log('Server Connected')
})

socket.on('disconnect', function () {
  console.log('Server Disconnected')
})

$('#butn').on('click', function (event) {
  event.preventDefault();
  if ($('#email').val() == '' || $('#password').val() == '') {
    return alert('All data is Mandatory')
  }
  $.post('/auth/login', {
    email: $('#email').val(),
    password: $('#password').val()
  }).then((res) => {
    token = res
    const socket = io.connect('http://localhost:8000', {
      query: `token=` + token
    });
  })
})

socket.on('done', function (data) {
  console.log(data);
  console.log(data.data)
  console.log(data.done)
  // Print the data.data somewhere...
});


socket.on('onplay', function (message) {
  console.log(message)
})


