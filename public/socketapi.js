$('#send').on('click', function (event) {
  event.preventDefault();
  if ($('#uuid').val() == '' || $('#iemi').val() == '' || $('#callid').val() == '' || $('#auth').val() == '') {
    return alert('All data is Mandatory')
  }
  socket.emit('data', {
    uuid: $('#uuid').val(),
    iemi: $('#iemi').val(),
    callid: $('#callid').val(),
    auth: $('#auth').val()
  });
  $('#form1').trigger("reset");
});

socket.on('callback', function (data) {
  console.log(data.done);
  console.log(data.auth)
  console.log(data.success)
  console.log(data)
});