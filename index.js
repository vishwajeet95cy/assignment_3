const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const chalk = require('chalk');
const dotenv = require('dotenv');
const logger = require('./config/logger')
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const morgan = require('morgan');
const MongoStore = require('connect-mongo')(session);
dotenv.config();
const app = express();
const path = require('path')
const authRoute = require('./routes/user');
const passport = require('passport');
require('./controllers/passort-setup')
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const socketAuth = require('socketio-auth');
const mobileData = require('./model/mobileModel')
const socketJWT = require('socketio-jwt');
const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Sample API',
      description: 'Sample API',
      servers: ["http://localhost:8000"]
    }
  },
  apis: ["./routes/user.js"]
};

const swaggerDocs = swaggerJsdoc(swaggerOptions);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
// app.use(require('express-status-monitor')());

const { authenticate, postAuthenticate, disconnect } = require('./controllers/user_auth');

app.use(bodyparser.urlencoded({ extended: false }))
app.use(bodyparser.json());
app.use(cors());
app.use(morgan('dev', { stream: logger.stream }));

mongoose.connect(process.env.db_host, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
  if (err) throw err;
  console.log(chalk.green('Database Connect'));
})

app.use(cookieParser());
app.use(session({
  secret: process.env.secret,
  resave: false,
  saveUninitialized: true,
  cookie: {
    secureProxy: true,
    // httpOnly: true,
    expires: 24 * 60 * 60
  },
  store: new MongoStore({
    mongooseConnection: mongoose.connection,
    ttl: 24 * 60 * 60
  })
}))
app.use(passport.initialize());
app.use(passport.session());

// app.use((req, res, next) => {
//   let oldSend = res.send;
//   res.send = function (data) {
//     return new Promise((resolve, raject) => {
//       oldSend.apply(res, arguments);
//       console.log(data)
//       return data;
//     }).then((data) => {

//     }).catch((err) => {
//       console.log(err)
//     });
//   }
//   next()
// })

app.use((req, res, next) => {
  console.log(req.session);
  console.log(req.cookies)
  console.log('***************')
  console.log(res.statusCode)
  next()
})

app.use('/auth', authRoute);

const checkUserLoggedIn = (req, res, next) => {
  req.user ? next() : res.redirect('/');
}

// const checkUserAlreadyLog = (req, res, next) => {
//   req.user ? next() : res.redirect('/good');
// }

// function isLoggedIn(req, res, next) {
//   if (req.isAuthenticated())
//     return next();
//   res.redirect('/');
// }

app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/public');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('socketio', io);
app.get('/', checkUserLoggedIn, function (req, res) {
  console.log(req.user)
  res.render('index.html');
  // res.send(`<h1> Welcome to server </h1>`)
});

app.get('/failed', (req, res, next) => {
  res.send('You failed for login')
});
app.get('/good', checkUserLoggedIn, (req, res, next) => {
  // console.log(req.user);
  // res.send(`Welcome to Mr. ${req.user}`);
  res.render('profile.html', { user: req.user });
});

io.use((socket, next) => {
  console.log(socket.handshake)
  let token = socket.handshake.query.token;
  if (!token) {
    return next();
  }
  return next(new Error('authentication error'));
});


// socketAuth(io, {
//   authenticate: authenticate,
//   postAuthenticate: postAuthenticate,
//   disconnect: disconnect,
//   timeout: 1000
// })

io.on('connection', (socket) => {
  console.log('Connecteds')
  socket.on('data', (data) => {
    if (!data.uuid || !data.iemi || !data.callid || !data.auth) {
      return console.log("Invalid Data");
    }
    new mobileData({
      uuid: data.uuid,
      iemi: data.iemi,
      callid: data.callid,
      auth: data.auth
    }).save((err, docs) => {
      if (err) { return console.log("error"); }
      socket.emit('callback', {
        success: "Data added successfully",
        url: 'http://' + data.hostname + ':' + process.env.PORT + data.originalUrl,
        auth: docs.auth,
        done: 'Done',
        data: docs
      })
      // socket.emit('callback', { done: 'Done', data: docs })
    })
  });

  socket.on('disconnect', () => {
    console.log('disconnect');
  })
})

// io.use(socketJWT.authorize({
//   secret: process.env.Token_secret,
//   handshake: true
// }));

// io.on('connection', (socket) => {
//   console.log('Io on method connect');
//   console.log(socket.decoded_token);
//   console.log('hello!', socket.decoded_token.name);
//   socket.emit('done', {
//     data: socket.decoded_token,
//     done: 'Done'
//   })
// })


server.listen(process.env.PORT, () => {
  console.log(`Server is listing at http://localhost:${process.env.PORT}`)
})




